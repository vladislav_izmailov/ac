///
///  Copyright © 2021 7Scale. All rights reserved.
///

import Foundation


struct HostModel {
    let host = "ota-buddy.ru"
    let port: UInt16 = 8000
}
