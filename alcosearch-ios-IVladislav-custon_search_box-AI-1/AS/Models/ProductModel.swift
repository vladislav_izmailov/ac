///
///  Copyright © 2021 7Scale. All rights reserved.
///

import Foundation


struct ProductModel: Codable {
    var id: UInt
    var title: String
    var volume: Float
    var prices: [PriceModel]
}

struct DetailedProductModel: Codable {
    var id: UInt
    var title: String
    var image: String
    var volume: Float
    var description: String
    var prices: [PriceModel]
}
