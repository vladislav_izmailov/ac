///
///  Copyright © 2021 7Scale. All rights reserved.
///

import Foundation


struct SearchProductsModel: Codable {
    var count: UInt
    var next: UInt?
    var previous: UInt?
    var results: [ProductModel]
}
