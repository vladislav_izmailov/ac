///
///  Copyright © 2021 7Scale. All rights reserved.
///

import Foundation


struct VendorModel: Codable {
    var id: UInt
    var title: String
    var phone: String
    var address: String
}
