///
///  Copyright © 2021 7Scale. All rights reserved.
///

import Foundation


struct PriceModel: Codable {
    var id: UInt
    var price: Float
    var count: UInt
    var vendorId: UInt
}
