///
///  Copyright © 2021 7Scale. All rights reserved.
///

import Foundation
import UIKit


class CustomTableCell : UITableViewCell {

    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var volume: UILabel!
    @IBOutlet weak var title: UILabel!

}
