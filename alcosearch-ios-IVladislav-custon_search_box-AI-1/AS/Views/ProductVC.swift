///
///  Copyright © 2021 7Scale. All rights reserved.
///

import UIKit


class ProductVC: UIViewController {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var volumeLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    var product: ProductModel?

    private lazy var apiMethods: APIMethods = {
        let hostModel = HostModel()
        let apiService = APIService(withHost: hostModel.host, port: hostModel.port)
        
        return APIMethods(withAPIService: apiService)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("VC called + \(self)")
        
        guard let id = product?.id else {
            print("Failed to extract product identifier")
            
            return
        }
        
        print("Going next")
        
        let _ = apiMethods.requestProductDetails(withId: id) { [weak self] model, error in
            guard let self = self else {
                return
            }

            if let error = error {
                print("Something went wrong: \(error)")

                return
            }

            guard let model = model else {
                print("There is no data in the model")

                return
            }
            
            DispatchQueue.global().async { [weak self] in
                if let data = try? Data(contentsOf: URL(string: model.image)!) {
                    if let image = UIImage(data: data) {
                        DispatchQueue.main.async {
                            self?.imageView.image = image
                        }
                    }
                }
            }
            
            self.titleLabel.text = model.title
            self.volumeLabel.text = String(model.volume) + "лр"
            self.priceLabel.text = String(model.prices.first!.price) + " руб"
            self.descriptionLabel.text = model.description
        }

    }
    
}
