///
///  Copyright © 2021 7Scale. All rights reserved.
///

import UIKit


class ProductsTVC: UITableViewController, UISearchResultsUpdating {
    
    // MARK: - Private variables and constants
    
    private lazy var apiMethods: APIMethods = {
        let hostModel = HostModel()
        let apiService = APIService(withHost: hostModel.host, port: hostModel.port)
        
        return APIMethods(withAPIService: apiService)
    }()
    
    private var products = [ProductModel]()


    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.rowHeight = 65
        let search = UISearchController(searchResultsController: nil)
        search.searchResultsUpdater = self
        search.obscuresBackgroundDuringPresentation = false
        search.searchBar.placeholder = "Type something here to search"
        navigationItem.searchController = search
        
        updateProducts()
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return products.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CustomTableCell", for: indexPath) as! CustomTableCell
        cell.accessoryType = .disclosureIndicator
        cell.title?.text = products[indexPath.row].title
        cell.volume?.text = String(products[indexPath.row].volume)
        
        var min: Float = Float(INT_MAX);
        var max: Float = 0;

        for price in products[indexPath.row].prices {
            if(price.price < min) {
                min = price.price
            }
            else if(price.price > max) {
                max = price.price
            }
        }

        cell.price?.text = "\(min) - \(max)"
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "Vendor", sender: products[indexPath.row])
        print("select row")
        tableView.deselectRow(at: indexPath, animated: true)
        print("deselect row")
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Vendor", let productVC = segue.destination as? ProductVC, let product = sender as? ProductModel {
            print("prepare + \(productVC)")
            productVC.product = product
        }
    }
    
    // MARK: - UISearchResultsUpdating
    
    func updateSearchResults(for searchController: UISearchController) {
        guard let text = searchController.searchBar.text else {
            return
        }
        
        updateProducts(withName: text)
    }
    
    // MARK: - Private methods
    
    private func updateProducts(withName name: String = "") {
        let _ = apiMethods.requestProduct(withName: name) { [weak self] model, error in
            guard let self = self else {
                return
            }

            if let error = error {
                print("Something went wrong: \(error)")

                return
            }

            guard let model = model else {
                print("There is no data in the model")

                return
            }
            
            self.products = model.results
            
            self.tableView.reloadData()
        }
    }
}

