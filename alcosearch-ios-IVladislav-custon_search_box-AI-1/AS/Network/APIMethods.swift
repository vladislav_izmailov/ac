///
///  Copyright © 2021 7Scale. All rights reserved.
///

import Foundation


class APIMethods {
    
    // MARK: - Private variables and constants
    
    let apiService: APIService
    
    // MARK: - Lifecycle
    
    init(withAPIService apiService: APIService) {
        self.apiService = apiService
    }
    
    // MARK: - Internal methods
    
    func requestProduct(withName name: String, _ completion: @escaping (SearchProductsModel?, Error?) -> Void) -> Bool {
        return apiService.requestSearch(withTerm: "search=\(name)", query: true, path: "api/search") { [weak self] data, error in
            guard let self = self else {
                return
            }
            
            self.processResponse(data, error, completion)
        }
    }

    func requestProductDetails(withId id: UInt, _ completion: @escaping (DetailedProductModel?, Error?) -> Void) -> Bool {
        return apiService.requestSearch(withTerm: String(id), query: false, path: "api/product") { [weak self] data, error in
            guard let self = self else {
                return
            }
            
            self.processResponse(data, error, completion)
        }
    }

    func requestVendor(withId id: UInt, _ completion: @escaping (VendorModel?, Error?) -> Void) -> Bool {
        return apiService.requestSearch(withTerm: String(id), query: false, path: "api/vendor") { [weak self] data, error in
            guard let self = self else {
                return
            }
            
            self.processResponse(data, error, completion)
        }
    }
    
    // MARK: - Private methods
    
    private func processResponse<T: Decodable>(_ data: Data?, _ error: Error?, _ completion: @escaping (T?, Error?) -> Void) {
        if let error = error {
            completion(nil, error)
            
            return
        }
        
        guard let data = data else {
            completion(nil, nil)

            return
        }
        
        do {
            let jsonDecoder = JSONDecoder()
            let result = try jsonDecoder.decode(T.self, from: data)
            
            completion(result, nil)
        } catch let error as NSError {
            completion(nil, error)
        }
    }

}
