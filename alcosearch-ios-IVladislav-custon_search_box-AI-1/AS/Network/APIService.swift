///
///  Copyright © 2021 7Scale. All rights reserved.
///

import UIKit


class APIService {

    // MARK: - Private variables
    
    private let host: String
    private let port: UInt16
    
    private let defaultSession = URLSession(configuration: .default)
    
    private var dataTask: URLSessionDataTask?
    
    // MARK: - Lifecycle

    init(withHost host: String, port: UInt16) {
        self.host = host
        self.port = port
    }
    
    // MARK: - Internal methods
    
    func requestSearch(withTerm term: String, query: Bool, path: String, _ completion: @escaping (Data?, Error?) -> Void) -> Bool {
        dataTask?.cancel()
        
        guard var urlComponents = URLComponents(string: "http://\(host):\(port)/\(path)/\(query == true ? "" : term)") else {
            return false
        }
        
        if query == true {
            urlComponents.query = term
        }
        
        guard let url = urlComponents.url else {
            return false
        }
        
        dataTask = defaultSession.dataTask(with: url) { [weak self] data, response, error in
            defer {
                self?.dataTask = nil
            }
            
            if let error = error {
                completion(nil, error)
                
                return
            }
            
            if let data = data, let response = response as? HTTPURLResponse, response.statusCode == 200 {
                DispatchQueue.main.async {
                    completion(data, nil)
                }
            } else {
                // TODO: - Right now just abort execution!
                fatalError()
            }
        }
        
        dataTask?.resume()
        
        return true
    }
    
}
